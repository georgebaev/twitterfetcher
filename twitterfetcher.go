// Package twitterfetcher provides functionality to retrieve Twitter's tweet data using the Application-only authentication.
// You could find more information at https://dev.twitter.com/docs/auth/application-only-auth .
package twitterfetcher

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"github.com/mreiferson/go-httpclient"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// TwitterUserAgent sets the user agent for the Twitter API GET request.
const TwitterUserAgent string = "Twitter Fetcher OAuth Functionality"

// TwitterApiURL sets the URL for the Twitter API
const TwitterApiURL string = "https://api.twitter.com"

// TwitterPost contains the data for a tweet which was retreived by the Twitter API.
type TwitterPost struct {
	Id        uint64 `json:"id"`
	CreatedAt string `json:"created_at"`
	Favorites uint64 `json:"favorite_count"`
	Retweets  uint64 `json:"retweet_count"`
	Text      string `json:"text"`
}

// ValidateURL validates the tweet URL.
// For example, a valid tweet URL is https://twitter.com/NatGeo/status/406603906547195904 .
func (tp *TwitterPost) ValidateURL(turl string) error {
	u, err := url.Parse(turl)

	if err != nil {
		return err
	}

	// check for scheme and host
	if u.Scheme != "https" || u.Host != "twitter.com" {
		return errors.New("Invalid Scheme or Host")
	}

	// check the path
	if len(u.Path) == 0 {
		return errors.New("Invalid Path")
	}

	splitpath := strings.Split(u.Path, "/")

	if splitpath[2] != "status" {
		return errors.New("Invalid status")
	}

	tweetId, err := strconv.ParseUint(splitpath[3], 10, 64)

	if err != nil {
		return err
	}

	tp.Id = tweetId

	return nil
}

func encodeKeys(consumerKey string, consumerSecret string) string {
	encodedkey := url.QueryEscape(consumerKey)
	encodedsecret := url.QueryEscape(consumerSecret)
	credentials := []string{encodedkey, encodedsecret}
	return base64.StdEncoding.EncodeToString([]byte(strings.Join(credentials, ":")))
}

// GetBearerToken retrieves a valid token for the Twitter API request.
func GetBearerToken(consumerKey string, consumerSecret string) (string, error) {
	postData := []byte("grant_type=client_credentials")

	token, err := processToken(consumerKey, consumerSecret, postData, "token")
	if err != nil {
		return "", err
	}

	return token, nil
}

// InvalidateBearerToken invalidates the bearer token retrieved by the GetBearerToken function.
func InvalidateBearerToken(token string, consumerKey string, consumerSecret string) error {
	postData := []byte(strings.Join([]string{"access_token", token}, "="))

	t, err := processToken(consumerKey, consumerSecret, postData, "invalidate_token")

	if err != nil {
		return err
	}

	if t != token {
		return errors.New("Tokens do not match")
	}

	return nil
}

func processToken(consumerKey string, consumerSecret string, postData []byte, resource string) (string, error) {
	client := twitterClient()

	encodedKeys := encodeKeys(consumerKey, consumerSecret)
	url := strings.Join([]string{TwitterApiURL, "oauth2", resource}, "/")

	req, err := http.NewRequest("POST", url, bytes.NewReader(postData))

	if err != nil {
		return "", err
	}

	req.Host = "api.twitter.com"
	req.Header.Add("User-Agent", TwitterUserAgent)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
	req.Header.Add("Content-Length", strconv.Itoa(len(postData)))
	req.Header.Add("Authorization", strings.Join([]string{"Basic", encodedKeys}, " "))

	resp, err := client.Do(req)

	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	dec := json.NewDecoder(resp.Body)

	if dec == nil {
		err = errors.New("Failed to start decoding JSON data")
		return "", err
	}

	json_map := make(map[string]interface{})

	err = dec.Decode(&json_map)
	if err != nil {
		return "", err
	}

	access_token := json_map["access_token"].(string)

	return access_token, nil
}

// HttpGet makes a GET request to the Twitter API using the validation token and a resource such as "statuses/show" .
// The received JSON data is unmarshaled in the TwitterPost instance.
func HttpGet(token string, resource string, tp *TwitterPost) error {
	client := twitterClient()

	tweetIdJson := strconv.FormatUint(tp.Id, 10) + ".json"

	url := strings.Join([]string{TwitterApiURL, "1.1", resource, tweetIdJson}, "/")

	req, err := http.NewRequest("GET", url, nil)

	if err != nil {
		return err
	}

	req.Header.Add("User-Agent", TwitterUserAgent)
	req.Header.Add("Authorization", strings.Join([]string{"Bearer ", token}, " "))

	resp, err := client.Do(req)

	if err != nil {
		return err
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return err
	}

	err = json.Unmarshal([]byte(body), tp)

	return nil
}

func twitterClient() *http.Client {
	transport := &httpclient.Transport{
		ConnectTimeout:        1 * time.Second,
		RequestTimeout:        10 * time.Second,
		ResponseHeaderTimeout: 5 * time.Second,
	}

	defer transport.Close()

	client := &http.Client{Transport: transport}

	return client
}
