package twitterfetcher

import (
	"testing"
)

func TestTwitterPostValidateURL(t *testing.T) {
	tp := TwitterPost{}

	InvalidURLs := make([]string, 5)
	InvalidURLs[0] = "test"
	InvalidURLs[1] = "https://google.com"
	InvalidURLs[2] = "https://twitter.com"
	InvalidURLs[3] = "https://twitter.com/user/1/2"
	InvalidURLs[4] = "https://twitter.com/gf_tech/status/non_valid)numeric"

	for _, u := range InvalidURLs {
		if err := tp.ValidateURL(u); err == nil {
			t.Errorf("Invalid Twitter Post URL '%s' passed as valid", u)
		}
	}

	ValidURLs := make([]string, 2)
	ValidURLs[0] = "https://twitter.com/gf_tech/status/397161331406553088"
	ValidURLs[1] = "https://twitter.com/Dries/status/397154458200580096"

	for _, u := range ValidURLs {
		if err := tp.ValidateURL(u); err != nil {
			t.Errorf("Valid Twitter Post URL %s passed as invalid", err)
		}
	}
}
