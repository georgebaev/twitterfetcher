# Description
The `twitterfetcher` package contains functions which allow Twitter API requests to resources such as `statuses/show` using the [Application-only authentication](https://dev.twitter.com/docs/auth/application-only-auth).
# Example
```
#!go
package main

import (
    "bitbucket.org/georgebaev/twitterfetcher"
  "fmt"
)

func main() {
  consumerKey := "consumer_key"
  consumerSecret := "consumer_secret"

  tp := twitterfetcher.TwitterPost{}

  token, err := twitterfetcher.GetBearerToken(consumerKey, consumerSecret)

  if err != nil {
    fmt.Println(err.Error())
    return
  }
  urls := []string{
    "https://twitter.com/NatGeo/status/406568201536176128",
    "https://twitter.com/NatGeo/status/406435668265816064",
    "https://twitter.com/NatGeo/status/406108897301528579",
    "https://twitter.com/NatGeo/status/405857308947005440",
    "https://twitter.com/NatGeo/status/405692461324206080",
  }

  for i, posturl := range urls {
    go func(posturl string, i int) {
      if err := tp.ValidateURL(posturl); err != nil {
        fmt.Println(err.Error())
        return
      }

      err = twitterfetcher.HttpGet(token, "statuses/show", &tp)

      if err != nil {
        fmt.Println(err.Error())
        return
      }

      fmt.Println(i, tp)
    }(posturl, i)
  }

  twitterfetcher.InvalidateBearerToken(token, consumerKey, consumerSecret)
}
```
